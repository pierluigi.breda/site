---
title: "Delta3: Slim"
year: "2015"
draft: false
weight: 75
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:

photo:

web:

description: Graphic/Layout/Products renderings/Drawings/Illustrations
featured_image: "delta3-slim-2015-001.jpg"
template_video: ""
video_link: ""
---
Graphic, layout, products renderings, drawings, illustrations.
