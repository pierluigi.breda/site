---
title: "Dorsal: Elisir mattress"
year: "2014"
draft: false
weight: 35
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering for catalog
featured_image: "dorsal-mattresses-2014-001.jpg"
template_video: ""
video_link: ""
---
Product rendering; mattress models with sections.

Agency: Corvinogualandi
