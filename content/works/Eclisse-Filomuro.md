---
title: "Eclisse: Filomuro"
year: "2010"
draft: false
weight: 2
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering of a new door model
featured_image: "eclisse-filomuro-2010-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling and rendering.
