---
title: "Eclisse: Silk"
year: "2014"
draft: false
weight: 7
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering new of new door models
featured_image: "eclisse-silk-2014-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling and rendering.
