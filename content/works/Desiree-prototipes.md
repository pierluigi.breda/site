---
title: "Desiree: prototipes"
year: "2007"
draft: true
weight: 70
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D modeling and rendering sofas prototipes
featured_image: "desiree-prototypes-2007-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering of a sofa.

Design: Arch. Edoardo Gherardi
