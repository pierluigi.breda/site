---
title: "Gmp: E series"
year: "2012"
draft: false
weight: 12
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: Product video of a flatwork ironer
featured_image: "gmp-flatwork-ironers-e-2012-001.jpg"
template_video: "yes"
video_link: "637096027"
---
Concept, 3D modeling, 3D animation, rendering, video editing.
