---
title: "Eclisse: home renovation"
year: "2012"
draft: false
weight: 5
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image for the house renovation
featured_image: "eclisse-renovate-2012-001.jpg"
template_video: ""
video_link: ""
---
Advertising image for the house renovation, ideation, 3D modeling and rendering.
