---
title: "Breda Isabella: brochure"
year: "2018"
draft: false
weight: 85
works:
  - 0
3d-cgi:

video:

graphic:
  - 0
design:

photo:

web:

description: Graphic layout
featured_image: "breda-isabella-brochure-2018-001.jpg"
template_video: ""
video_link: ""
---
Concept and realization of the graphic layout.
