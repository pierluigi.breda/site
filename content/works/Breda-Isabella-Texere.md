---
title: "Breda Isabella: Texere"
year: "2015"
draft: false
weight: 78
works:
  - 0
3d-cgi:

video:

graphic:
  - 0
design:

photo:

web:

description: Graphic layout
featured_image: "breda-isabella-texere-2015-001.jpg"
template_video: ""
video_link: ""
---
Concept and realization of the graphic layout.
