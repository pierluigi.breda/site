---
title: "Barausse: door models"
year: "2009"
draft: false
weight: 19
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Image creation for catalog
featured_image: "barausse-catalog-2009-001.jpg"
template_video: ""
video_link: ""
---
Product / interior modeling and rendering of various door models.
