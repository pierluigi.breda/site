---
title: "Mariemi: eyeglasses rendering"
year: "2008"
draft: false
weight: 38
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Eyeglass prototype renderings
featured_image: "mariemi-zic-2008-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling, rendering.
