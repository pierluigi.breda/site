---
title: "Gmp: flatwork ironers video"
year: "2009"
draft: false
weight: 74
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: 
featured_image: "gmp-flatwork-ironers-general-2009-001.jpg"
template_video: "yes"
video_link: "637096774"
---
Concept, 3D modeling, 3D animation, rendering, video editing.
