---
title: "De Faveri: Ewoluto system video"
year: "2008"
draft: false
weight: 59
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: 
featured_image: "de-faveri-ewoluto-video-2008-001.jpg"
template_video: "yes"
video_link: "637102749"
---
Concept, 3D modeling, 3D animation, rendering, video editing.
