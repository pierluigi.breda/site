---
title: "Nutcracker"
year: "2009"
draft: false
weight: 90
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D Modeling and rendering
featured_image: "borghi-nutcracker-2009-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
