---
title: "Demo Style: furniture"
year: "2007"
draft: false
weight: 96
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D Modeling and rendering
featured_image: "demo-style-catalog-2007-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
