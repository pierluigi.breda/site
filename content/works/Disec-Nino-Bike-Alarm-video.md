---
title: "Disec: Nino Bike Alarm video"
year: "2019"
draft: false
weight: 29
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: Product video of a smart lock system
featured_image: "disec-nino-video-2019-001.jpg"
template_video: "yes"
video_link: "637098817"
---
Product video.
Nino bike alarm is an alarm system for bicycles, developed with the latest technologies. Nino keeps an eye on your bicycle by alerting you on your smartphone in case of attempted theft. Its design makes it suitable for all types of bicycle.
