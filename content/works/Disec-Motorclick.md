---
title: "Disec: Motorclick"
year: "2021"
draft: false
weight: 56
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:

photo:

web:

description: Product rendering of a smart relay
featured_image: "disec-motorclick-2021-001.jpg"
template_video: ""
video_link: ""
---
Product rendering.
Product instructions booklet.
Motorclick it’s a smart relay; a smart switch that can be controlled with smarphone, keypad or keyfob.
