---
title: "Mura: Giuditta video"
year: "2010"
draft: false
weight: 87
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: 
featured_image: "mura-giuditta-2010-001.jpg"
template_video: "yes"
video_link: "637104170"
---
Concept, 3D animation, rendering, video editing.
