---
title: "Delta3: Q-box"
year: "2013"
draft: false
weight: 102
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D Modeling and rendering
featured_image: "delta3-qbox-2013-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
