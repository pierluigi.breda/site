---
title: "Ke Gennius: adv"
year: "2011"
draft: false
weight: 62
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of a solar awning system
featured_image: "ke-outdoor1-2011-001.jpg"
template_video: ""
video_link: ""
---
Advertising image; 3D modeling and rendering.

Agency: Corvinogualandi
