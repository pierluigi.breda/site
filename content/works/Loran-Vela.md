---
title: "Loran: Vela"
year: "2010"
draft: false
weight: 32
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of a boat
featured_image: "loran-adv-vela-2010-001.jpg"
template_video: ""
video_link: ""
---
Advertising image; 3D modeling and rendering of a boat.

Agency: Corvinogualandi
