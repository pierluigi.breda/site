---
title: "Falmec: Marte"
year: "2014"
draft: false
weight: 36
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Rendering of an extractor hood
featured_image: "falmec-marte-2014-001.jpg"
template_video: ""
video_link: ""
---
Image rendering of an extractor hood.

Agency: Corvinogualandi
