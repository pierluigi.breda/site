---
title: "Falmec: Slide"
year: "2009"
draft: false
weight: 31
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Rendering of an extractor hood
featured_image: "falmec-slide-2009-001.jpg"
template_video: ""
video_link: ""
---
Image rendering of an extractor hood.

Agency: Corvinogualandi
