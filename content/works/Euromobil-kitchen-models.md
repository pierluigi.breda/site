---
title: "Euromobil: kitchen models"
year: "2010"
draft: false
weight: 77
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Renderings
featured_image: "euromobil-rendering-2010-001.jpg"
template_video: ""
video_link: ""
---
Previsualization renderings.

Design: Arch. Edoardo Gherardi
