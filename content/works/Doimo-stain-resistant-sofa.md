---
title: "Doimo: stain resistant sofa"
year: "2013"
draft: false
weight: 34
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D Modeling / rendering and compositing
featured_image: "doimo-adv-2013-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering of a wine glass, image editing and compositing.

Agency: Corvinogualandi
