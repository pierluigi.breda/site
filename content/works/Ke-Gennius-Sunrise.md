---
title: "Ke Gennius: Sunrise"
year: "2012"
draft: false
weight: 33
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of a solar awning system
featured_image: "ke-sunrise-2012-001.jpg"
template_video: ""
video_link: ""
---
Advertising image; 3D modeling and rendering.

Agency: Corvinogualandi
