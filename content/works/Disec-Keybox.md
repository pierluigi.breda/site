---
title: "Disec: Keybox"
year: "2021"
draft: false
weight: 10
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:

photo:

web:

description: Product rendering of a smart safe box
featured_image: "disec-keybox-2021-001.jpg"
template_video: ""
video_link: ""
---
Product rendering.
Product instructions booklet.
Keybox is a strong/durable box that can contain a key or a bunch of keys; it can be controlled via smartphone, keypad e keyfob.
