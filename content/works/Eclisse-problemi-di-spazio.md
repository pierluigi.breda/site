---
title: "Eclisse: problemi di spazio?"
year: "2008"
draft: false
weight: 18
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Creation of a mouse wheel
featured_image: "eclisse-adv-hamster-2008-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling, rendering and image editing of a mouse wheel.

Agency: Corvinogualandi
