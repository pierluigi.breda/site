---
title: "Ke Gennius: adv 2"
year: "2011"
draft: false
weight: 73
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of a solar awning system
featured_image: "ke-outdoor2-2011-001.jpg"
template_video: ""
video_link: ""
---
Advertising image; 3D modeling and rendering.

Agency: Corvinogualandi
