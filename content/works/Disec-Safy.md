---
title: "Disec: Safy"
year: "2019"
draft: false
weight: 8
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering and video of a smart motorbike disk lock
featured_image: "disec-safy-2019-001.jpg"
template_video: ""
video_link: ""
---
Product rendering.
The disk lock magnetic padlock that alerts your smartphone of an attempted theft.
