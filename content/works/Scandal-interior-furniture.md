---
title: "Scandal: interior furniture"
year: "2014"
draft: false
weight: 44
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Image creation for catalog
featured_image: "scandal-catalog-2014-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
