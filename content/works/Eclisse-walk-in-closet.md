---
title: "Eclisse: walk-in closet"
year: "2013"
draft: false
weight: 27
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image for the house renovation
featured_image: "eclisse-wardrobes-2013-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling and rendering.
