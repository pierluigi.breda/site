---
title: "Delta3: Open Space"
year: "2013"
draft: false
weight: 66
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:

photo:

web:

description: Graphic/Layout/Products renderings/Drawings/Illustrations
featured_image: "delta3-openspace-2013-001.jpg"
template_video: ""
video_link: ""
---
Graphic, layout, products renderings, drawings, illustrations.
