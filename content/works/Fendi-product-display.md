---
title: "Fendi: product display"
year: "2011"
draft: false
weight: 4
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:
  - 0
photo:

web:

description: Design of a glasses display
featured_image: "fendi-product-display-2011-001.jpg"
template_video: ""
video_link: ""
---
Design of a glasses display.
