---
title: "Eclisse: fireproof"
year: "2008"
draft: false
weight: 99
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering of a door model
featured_image: "eclisse-fireproof-2008-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling and rendering.
