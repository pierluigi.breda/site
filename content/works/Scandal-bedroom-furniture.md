---
title: "Scandal: bedroom furniture"
year: "2016"
draft: false
weight: 48
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D Modeling and rendering of a bed, chair and accessories
featured_image: "scandal-bedroom-2016-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering of a bed, chair and accessories.
