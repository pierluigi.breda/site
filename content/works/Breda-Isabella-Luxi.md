---
title: "Breda Isabella: Luxi"
year: "2016"
draft: false
weight: 14
works:
  - 0
3d-cgi:

video:

graphic:
  - 0
design:

photo:

web:

description: Graphic layout
featured_image: "breda-isabella-luxi-2016-001.jpg"
template_video: ""
video_link: ""
---
Concept and realization of the graphic layout.
