---
title: "Falmec: Laguna "
year: "2009"
draft: false
weight: 21
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of an extractor hood
featured_image: "falmec-laguna-2009-001.jpg"
template_video: ""
video_link: ""
---
Advertising image rendering of an extractor hood.

Agency: Corvinogualandi
