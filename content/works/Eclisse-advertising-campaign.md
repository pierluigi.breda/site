---
title: "Eclisse: advertising campaign"
year: "2011"
draft: false
weight: 23
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Creation of a series of images
featured_image: "eclisse-adv-2011-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
Creation of a series of images for the advertising campaign.

Agency: Gulliver
