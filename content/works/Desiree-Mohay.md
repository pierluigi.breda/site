---
title: "Desiree: Mohay"
year: "2007"
draft: false
weight: 58
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D modeling and rendering of a sofa
featured_image: "desiree-mohay-2007-001.jpg"
template_video: ""
video_link: ""
---
3D modeling and rendering of a sofa.

Design: Arch. Edoardo Gherardi
