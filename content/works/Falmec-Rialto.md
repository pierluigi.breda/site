---
title: "Falmec: Rialto"
year: "2009"
draft: false
weight: 60
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Advertising image of an extractor hood
featured_image: "falmec-rialto-2009-001.jpg"
template_video: ""
video_link: ""
---
Advertising image; 3D modeling and rendering.

Agency: Corvinogualandi
