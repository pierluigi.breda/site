---
title: "San Gallo: Oscuro"
year: "2020"
draft: false
weight: 22
works:
  - 0
3d-cgi:

video:

graphic:
  - 0
design:

photo:
  - 0
web:

description: Label design packaging and photos
featured_image: "san-gallo-oscuro-2020-001.jpg"
template_video: ""
video_link: ""
---
Label design packaging and photos.
