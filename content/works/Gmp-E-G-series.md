---
title: "Gmp: E / G series"
year: "2008"
draft: false
weight: 37
works:
  - 0
3d-cgi:

video:
  - 0
graphic:

design:

photo:

web:

description: Product video of a flatwork ironer
featured_image: "gmp-flatwork-ironers-eg-2008-001.jpg"
template_video: "yes"
video_link: "637095817"
---
Concept, 3D modeling, 3D animation, rendering, video editing.
