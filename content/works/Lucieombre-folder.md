---
title: "Lucieombre: folder"
year: "2008"
draft: false
weight: 49
works:
  - 0
3d-cgi:

video:

graphic:
  - 0
design:

photo:

web:

description: folding poster for furniture shop
featured_image: "lucieombre-poster-2008-001.jpg"
template_video: ""
video_link: ""
---
Folding poster design for furniture shop.
