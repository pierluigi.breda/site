---
title: "Disec: Nino Bike Alarm"
year: "2019"
draft: false
weight: 28
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering of a smart lock system
featured_image: "disec-nino-2019-001.jpg"
template_video: ""
video_link: ""
---
Product rendering.
Nino bike alarm is an alarm system for bicycles, developed with the latest technologies. Nino keeps an eye on your bicycle by alerting you on your smartphone in case of attempted theft. Its design makes it suitable for all types of bicycle.
