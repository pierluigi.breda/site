---
title: "Falmec: metal fish"
year: "2011"
draft: false
weight: 11
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Rendering and 3D modeling of a fish
featured_image: "falmec-fish-2011-001.jpg"
template_video: ""
video_link: ""
---
Rendering and 3D modeling of a metal fish.

Agency: Corvinogualandi
