---
title: "Eclisse: Extension sliding door"
year: "2008"
draft: false
weight: 81
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Product rendering of a door model
featured_image: "eclisse-double-extension-2008-001.jpg"
template_video: ""
video_link: ""
---
Concept, 3D modeling and rendering.
