---
title: "Montello: furniture"
year: "2011"
draft: false
weight: 47
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Image creation for catalog
featured_image: "montello-catalog-2011-001.jpg"
template_video: ""
video_link: ""
---
3D Modeling and rendering.
