---
title: "Fendi: invitation"
year: "2011"
draft: false
weight: 24
works:
  - 0
3d-cgi:
  - 0
video:

graphic:
  - 0
design:
  - 0
photo:

web:

description: Design for a graphic invitation card
featured_image: "fendi-invitation-2011-001.jpg"
template_video: ""
video_link: ""
---
Design for a graphic invitation card.
