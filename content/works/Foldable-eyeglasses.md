---
title: "Foldable eyeglasses"
year: "2006"
draft: false
weight: 1
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:
  - 0
photo:

web:

description: Research design and patent
featured_image: "breda-eyeglasses-2006-001.jpg"
template_video: ""
video_link: ""
---
Folding reading glasses, pocket-sized and very sturdy. They need no case because the symmetrical ear pieces create a shell when they are folded over one another thanks to a special ball joint that makes it possible to reassemble the components if they accidentally break off. It needs no screws or metal components: this makes the glasses lightweight, inexpensive to manufacture, leaving very little recyclable material waste at the end of the manufacturing process.
Zic is a patented glasses.
