---
title: "Stelbi: Ntouch System"
year: "2013"
draft: false
weight: 13
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: Rendering for the Ntouch system catalogue
featured_image: "stelbi-ntouch-2013-001.jpg"
template_video: ""
video_link: ""
---
Product rendering, 3D modeling.
