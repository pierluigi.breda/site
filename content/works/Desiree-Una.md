---
title: "Desiree: Una"
year: "2007"
draft: false
weight: 30
works:
  - 0
3d-cgi:
  - 0
video:

graphic:

design:

photo:

web:

description: 3D modeling and rendering of a chair
featured_image: "desiree-una-2007-001.gif"
template_video: ""
video_link: ""
---
3D modeling and rendering of a chair.

Design: Arch. Edoardo Gherardi
